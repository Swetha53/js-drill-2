function flat(arr) {
    let flattenedArray = [];
    arr.forEach(el => {
      if(Array.isArray(el)){
        const result = flat(el);
        result.forEach(el => flattenedArray.push(el));
      } else {
        flattenedArray.push(el);
      }
    });
    return flattenedArray;
  }
  
  
  module.exports=flat